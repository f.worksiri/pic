import React from 'react';
import { View, ScrollView } from 'react-native';
import styles from './styles';
import { TitleLabel, BtnFilter, ImgCollection, HrLine } from '../../components';

export default function Trending() {
  return (
    <View style={styles.trendingContainerLayout}>
      <View style={styles.titleLayout}>
        <TitleLabel label="Trending" />
      </View>
      <View style={styles.filterLayout}>
        <ScrollView horizontal>
          <View style={styles.itemFilterWrap}>
            <BtnFilter label="Monthly" />
          </View>
          <View style={styles.itemFilterWrap}>
            <BtnFilter label="Weekliy" />
          </View>
          <View style={styles.itemFilterWrap}>
            <BtnFilter label="Thailand" />
          </View>
        </ScrollView>
      </View>
      <ScrollView>
        <View style={styles.cateLayout}>
          <ImgCollection
            title="Top posts by engagement"
            iconName="thumbs-up"
            iconColor="#696CE0"
            iconType="feather"
            iconSize={30}
            data={[
              {
                imgPath: require('../../assets/images/test93x93.jpg'),
                imgLabel: '1k votes',
              },
              {
                imgPath: require('../../assets/images/test93x93.jpg'),
                imgLabel: '5.2k votes',
              },
            ]}
          />
        </View>
        <HrLine />
        <View style={styles.cateLayout}>
          <ImgCollection
            title="Top posts rated true"
            iconName="user"
            iconColor="#61CAAC"
            iconType="feather"
            iconSize={30}
            data={[
              {
                imgPath: require('../../assets/images/test93x93.jpg'),
                imgLabel: 'True 2k',
              },
              {
                imgPath: require('../../assets/images/test93x93.jpg'),
                imgLabel: 'False 2.5k',
              },
            ]}
          />
        </View>
        <HrLine />
        <View style={styles.cateLayout}>
          <ImgCollection
            title="Top users rated true"
            iconName="volume-2"
            iconColor="#696CE0"
            iconType="feather"
            iconSize={30}
            imgLabelColor="#378EED"
            data={[
              {
                imgPath: require('../../assets/images/test93x93.jpg'),
                imgLabel: '@Username',
              },
              {
                imgPath: require('../../assets/images/test93x93.jpg'),
                imgLabel: '@Username',
              },
            ]}
          />
        </View>
        <HrLine />
        <View style={styles.cateLayout}>
          <ImgCollection
            title="Top voters"
            iconName="aperture"
            iconColor="#61CAAC"
            iconType="feather"
            iconSize={30}
            imgLabelColor="#378EED"
            data={[
              {
                imgPath: require('../../assets/images/test93x93.jpg'),
                imgLabel: '@Username',
              },
              {
                imgPath: require('../../assets/images/test93x93.jpg'),
                imgLabel: '@Username',
              },
            ]}
          />
        </View>
      </ScrollView>
    </View>
  );
}
