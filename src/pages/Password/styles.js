import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  passwordContainerLayout: {
    marginTop: 88,
    flex: 1,
    paddingVertical: 8,
    paddingLeft: 24,
  },
});

export default styles;
