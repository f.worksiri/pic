import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  profileContainerLayout: {
    marginTop: 88,
    flex: 1,
    paddingVertical: 30,
    paddingHorizontal: 16,
  },
  avatarLayout: {
    marginBottom: 16,
  },
  scoreLayout: {
    flexDirection: 'row',
    marginBottom: 12,
    marginHorizontal: -8,
  },
  itemScoreLayout: {
    marginHorizontal: 8,
  },
  tabLayout: {
    flex: 1,
  },
});

export default styles;
