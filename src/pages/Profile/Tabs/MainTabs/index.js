import React, { useState } from 'react';
import { View, Dimensions, Text } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import styles from './styles';
import { Posts, Replies, VoteLike, Bookmark } from '../';

const initialLayout = { width: Dimensions.get('window').width };

export default function MainTabs() {
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: 'posts', title: 'POSTS' },
    { key: 'replies', title: 'REPLIES' },
    { key: 'votelike', title: 'VOTE/LIKES' },
    { key: 'bookmark', title: 'BOOKMARK' },
  ]);

  const renderTabBar = (props) => (
    <TabBar
      {...props}
      indicatorContainerStyle={{
        borderBottomWidth: 1,
        borderBottomColor: '#E2E4EF',
      }}
      indicatorStyle={{
        backgroundColor: '#696CE0',
        borderTopLeftRadius: 200,
        borderTopRightRadius: 200,
        height: 3,
        bottom: -1,
      }}
      style={{ backgroundColor: 'transparent' }}
      renderLabel={({ route, focused, color }) => (
        <Text style={focused ? styles.tabLabelF : styles.tabLabel}>{route.title}</Text>
      )}
    />
  );

  const renderScene = ({ route }) => {
    switch (route.key) {
      case 'posts':
        return (
          <View style={styles.tabContent}>
            <Posts />
          </View>
        );
      case 'replies':
        return (
          <View style={styles.tabContent}>
            <Replies />
          </View>
        );
      case 'votelike':
        return (
          <View style={styles.tabContent}>
            <VoteLike />
          </View>
        );
      case 'bookmark':
        return (
          <View style={styles.tabContent}>
            <Bookmark />
          </View>
        );
      default:
        return null;
    }
  };

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
      renderTabBar={renderTabBar}
    />
  );
}
