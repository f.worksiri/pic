import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  postContainerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: -4,
    marginVertical: -4,
  },
  itemLayout: {
    marginHorizontal: 4,
    marginVertical: 4,
  },
});

export default styles;
