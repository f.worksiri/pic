import React from 'react';
import { View } from 'react-native';
import styles from './styles';
import { AvartarHorizontal, ScoreText } from '../../components';
import MainTabs from './Tabs/MainTabs';

export default function Profile(props) {
  const { navigation } = props;

  const handlePressSetting = () => {
    navigation.navigate('Setting');
  };

  return (
    <View style={styles.profileContainerLayout}>
      <View style={styles.avatarLayout}>
        <AvartarHorizontal
          label1="Username"
          label2="@username"
          imgPath={{ uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg' }}
          handlePressSetting={handlePressSetting}
        />
      </View>
      <View style={styles.scoreLayout}>
        <View style={styles.itemScoreLayout}>
          <ScoreText label1="100" label2="Following" />
        </View>
        <View style={styles.itemScoreLayout}>
          <ScoreText label1="10" label2="Followers" />
        </View>
      </View>
      <View style={styles.tabLayout}>
        <MainTabs />
      </View>
    </View>
  );
}
