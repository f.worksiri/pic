export { default as Home } from './Home';
export { default as Trending } from './Trending';
export { default as Profile } from './Profile';
export { default as Setting } from './Setting';
export { default as PersonalInfomation } from './PersonalInfomation';
export { default as Password } from './Password';
