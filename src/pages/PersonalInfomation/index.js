import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';
import { AvatarVeticalEdit, InputTextHorizontal } from '../../components';

export default function PersonalInfomation() {
  return (
    <View style={styles.personalContainerLayout}>
      <View style={styles.avatarLayout}>
        <AvatarVeticalEdit
          label1="Change profile photo"
          imgPath={{ uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg' }}
        />
      </View>
      <View style={styles.formContainerLayout}>
        <View style={styles.formBlockLayout}>
          <InputTextHorizontal label="Name" labelWidth={80} placeholder="docter strang" />
          <InputTextHorizontal label="Username" labelWidth={80} placeholder="iron man" />
        </View>
        <View style={styles.formBlockLayout}>
          <View style={styles.titleWrap}>
            <Text style={styles.titleFormGroup}>Private information</Text>
          </View>
          <InputTextHorizontal label="Email" labelWidth={80} placeholder="mail@mail.com" />
          <InputTextHorizontal label="Phone" labelWidth={80} placeholder="+99 9999 9999" />
        </View>
      </View>
    </View>
  );
}
