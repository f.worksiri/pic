import React from 'react';
import { View } from 'react-native';
import styles from './styles';
import { AvatarVetical, MenuItem } from '../../components';

export default function Setting(props) {
  const { navigation } = props;
  const handlePressMenu = (key) => {
    switch (key) {
      case 'personal_information':
        navigation.navigate('Personal Information');
        break;
      case 'password':
        navigation.navigate('Password');
        break;
      default:
        break;
    }
  };

  return (
    <View style={styles.settingContainerLayout}>
      <View style={styles.avatarLayout}>
        <AvatarVetical
          label1="Username"
          label2="@username"
          imgPath={{ uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg' }}
        />
      </View>
      <View style={styles.menuLayout}>
        <MenuItem
          iconLName="person"
          iconLType="material"
          iconRName="chevron-right"
          iconRType="feather"
          text="Personal Information"
          handlePress={() => handlePressMenu('personal_information')}
        />
        <MenuItem
          iconLName="lock"
          iconLType="material"
          iconRName="chevron-right"
          iconRType="feather"
          text="Password"
          handlePress={() => handlePressMenu('password')}
        />
        <MenuItem
          iconLName="translate"
          iconLType="material"
          iconRName="chevron-right"
          iconRType="feather"
          text="Language"
        />
        <MenuItem
          iconLName="help"
          iconLType="material"
          iconRName="chevron-right"
          iconRType="feather"
          text="Help"
        />
        <MenuItem
          iconLName="assignment"
          iconLType="material"
          iconRName="chevron-right"
          iconRType="feather"
          text="Terms & Conditions"
        />
      </View>
    </View>
  );
}