import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  settingContainerLayout: {
    marginTop: 88,
    flex: 1,
    paddingVertical: 30,
  },
  avatarLayout: {
    marginBottom: 26,
  },
  menuLayout: {
    paddingLeft: 30,
  },
});

export default styles;
