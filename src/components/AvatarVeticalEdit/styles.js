import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  avatarWrap: {
    marginBottom: 8,
  },
  label1: {
    fontSize: 16,
    fontWeight: '600',
    color: '#378EED',
  },
});

export default styles;
