import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';
import { Avatar } from 'react-native-elements';
import { Icon } from 'react-native-elements';

export default function AvartarHorizontal(props) {
  const { imgPath, label1, label2, handlePressSetting } = props;
  return (
    <View style={styles.container}>
      <View style={styles.leftCol}>
        <Avatar rounded source={imgPath} size={40} />
      </View>
      <View style={styles.rightCol}>
        <View style={styles.labelWrap}>
          <Text style={styles.label1}>{label1}</Text>
          <TouchableOpacity onPress={handlePressSetting}>
            <Icon name="settings" type="material" size={14} color="#4E5B7E" />
          </TouchableOpacity>
        </View>
        <Text style={styles.label2}>{label2}</Text>
      </View>
    </View>
  );
}
