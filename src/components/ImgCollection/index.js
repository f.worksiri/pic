import React from 'react';
import { View, Text, ScrollView, Image } from 'react-native';
import styles from './styles';
import { Icon } from 'react-native-elements';

export default function ImgCollection(props) {
  const {
    title,
    iconType,
    iconName,
    iconColor,
    iconSize,
    imgLabelColor = "#7180AC",
    data,
  } = props;
  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.label}>{title}</Text>
        <Icon name={iconName} color={iconColor} type={iconType} size={iconSize} />
      </View>
      <View style={styles.imgWrap}>
        <ScrollView horizontal>
          {data &&
            data.map((e, i) => (
              <View key={i} style={styles.imgContainer}>
                <Image source={e.imgPath} style={{ width: 93, height: 93 }} />
                <Text style={[styles.imgLabel, { color: imgLabelColor }]}>{e.imgLabel}</Text>
                <View style={styles.rankNo}>
                  <Text style={styles.textRankNo}>{i + 1}</Text>
                </View>
              </View>
            ))}
        </ScrollView>
      </View>
    </View>
  );
}
