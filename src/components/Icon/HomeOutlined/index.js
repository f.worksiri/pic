import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function HomeOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 18;
  const viewBoxHeight = 18;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M.643 18h5.143a.643.643 0 00.643-.643V13.5A2.574 2.574 0 019 10.928a2.574 2.574 0 012.571 2.572v3.857c0 .355.288.643.643.643h5.143a.643.643 0 00.643-.643V7.071a.64.64 0 00-.251-.51L9.392.134a.643.643 0 00-.784 0L.25 6.562A.642.642 0 000 7.07v10.286c0 .355.287.643.643.643zm.643-10.613L9 1.454l7.714 5.933v9.327h-3.857V13.5A3.861 3.861 0 009 9.643 3.861 3.861 0 005.143 13.5v3.214H1.286V7.387z"
        fill={color}
      />
    </Svg>
  );
}
