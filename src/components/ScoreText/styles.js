import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  },
  label1: {
    fontSize: 16,
    fontWeight: '500',
    marginRight: 4,
    color: '#7180AC'
  },
  label2: {
    fontSize: 16,
    fontWeight: '500',
    color: '#7180AC'
  },
});

export default styles;
