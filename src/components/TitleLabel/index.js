import React from 'react';
import { Text } from 'react-native';
import styles from './styles';

export default function TitleLabel(props) {
  const { label } = props;
  return <Text style={styles.title}>{label}</Text>;
}
