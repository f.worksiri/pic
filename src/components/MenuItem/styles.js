import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  leftCol: {},
  rightCol: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 21,
    paddingRight: 30,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#E2E4EF'
  },
  text: {
    fontSize: 16,
    fontWeight: "500"
  },
});

export default styles;
