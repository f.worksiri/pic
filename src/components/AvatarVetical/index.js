import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';
import { Avatar } from 'react-native-elements';

export default function AvatarVetical(props) {
  const { imgPath, label1, label2 } = props;
  return (
    <View style={styles.container}>
      <View style={styles.avatarWrap}>
        <Avatar rounded source={imgPath} size={40} />
      </View>
      <Text style={styles.label1}>{label1}</Text>
      <Text style={styles.label2}>{label2}</Text>
    </View>
  );
}
