import React from 'react';
import { View } from 'react-native';
import styles from './styles';

export default function HrLine(props) {
  const { mgVertical = 16 } = props;
  return <View style={[styles.line, { marginVertical: mgVertical }]} />;
}
