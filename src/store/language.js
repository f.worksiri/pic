import { thunk, action } from 'easy-peasy';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import _ from 'lodash';

import i18n, { languages } from '../locales/i18n';

const initialState = {
  language: 'en',
};

const languageModel = {
  ...initialState,
  // action
  reset: action((state) => ({
    ...initialState,
  })),
  setLanguage: action((state, payload) => {
    state.language = payload;
  }),
  // thunk
  getLanguageFromStorage: thunk(async (actions, payload) => {
    const language = await AsyncStorage.getItem('language');
    if (language) {
      i18n.changeLanguage(language);
      actions.setLanguage(language);
      const languageObj = _.find(languages, { key: language });
      moment.locale(languageObj.momentKey);
    }
    return language;
  }),
  switchLanguage: thunk(async (actions, payload) => {
    const languageObj = _.find(languages, { key: payload });
    moment.locale(languageObj.momentKey);

    i18n.changeLanguage(payload);
    actions.setLanguage(payload);

    await AsyncStorage.setItem('language', payload);
    await actions.changeAppLanguage({ id: 0 });
  }),
};

export default languageModel;
