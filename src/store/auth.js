import { thunk, action } from 'easy-peasy';
import AsyncStorage from '@react-native-async-storage/async-storage';
import _ from 'lodash';

const initialState = {
  isLogin: false,
  token: null,
  profile: {
    id: null,
    fullname: null,
    firstname: null,
    lastname: null,
    phone: null,
    email: null,
    profileImage: null,
  },
};

const authModel = {
  ...initialState,
  // action
  reset: action((state) => ({
    ...initialState,
  })),
  setLogin: action((state, payload) => {
    state.isLogin = payload;
  }),
  setToken: action((state, payload) => {
    state.token = payload;
  }),
  setProfile: action((state, payload) => {
    state.profile = payload;
  }),
  // thunk
  register: thunk(async (actions, payload) => {
    const { username, password } = payload;
  }),
  login: thunk(async (actions, payload, { getStoreActions }) => {
    const { username, password } = payload;
  }),
  logout: thunk(async (actions, payload, { getStoreActions }) => {
    await AsyncStorage.clear();
    await getStoreActions().language.reset();
    await getStoreActions().auth.reset();
  }),
  getProfile: thunk(async (actions, payload, { getStoreActions }) => {
    const token = await AsyncStorage.getItem('token');
  }),
  updateProfile: thunk(async (actions, payload, { getState }) => {}),
};

export default authModel;
