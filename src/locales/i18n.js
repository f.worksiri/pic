import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import _ from 'lodash';
import moment from 'moment';
// import 'moment/locale/ja';

import enCommon from './en/common';
// import jpCommon from './jp/common';

const defaultLanguage = 'en';
const languages = [
  { key: 'en', momentKey: 'en', translation: enCommon },
  // { key: 'jp', momentKey: 'ja', translation: jpCommon },
];
const languageCodes = _.map(languages, (language) => language.key);

const languageObj = _.find(languages, { key: defaultLanguage });
moment.locale(languageObj.momentKey);

const resources = _.reduce(
  languages,
  (result, value, key) => {
    return {
      ...result,
      [value.key]: {
        translation: value.translation,
      },
    };
  },
  {},
);

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: defaultLanguage,
    // fallbackLng: defaultLanguage,
    keySeparator: false, // we do not use keys in form messages.welcome
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;

export { defaultLanguage, languages, languageCodes };
